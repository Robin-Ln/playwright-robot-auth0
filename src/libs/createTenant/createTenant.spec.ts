import { test } from "@playwright/test";
import { login } from "~libs/login";
import { createTenant } from "~libs/createTenant";
import { config } from "dotenv";
import { faker } from "@faker-js/faker";

config();

test("Click on cancel button", async ({ page }) => {
  await login(page, {
    url: process.env.AUTH0_URL,
    username: process.env.AUTH0_USERNME,
    password: process.env.AUTH0_PASSWORD,
  });

  await createTenant(page, {
    name: `robot-${faker.internet.domainWord()}`,
    region: "EU",
    env: "Development",
    complete: false,
  });
});
