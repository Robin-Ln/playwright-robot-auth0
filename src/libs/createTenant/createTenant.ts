import { Page } from "playwright";
import { EnvironmentTagName, RegionName } from "~commons/type";

type CreateTenantProps = {
  name: string;
  complete: boolean;
  region: RegionName;
  env: EnvironmentTagName;
};

export const createTenant = async (
  page: Page,
  { name, complete, region, env }: CreateTenantProps
) => {
  await page.getByTestId("tenant-menu-trigger").click();
  await page.locator("[data-guide-id=tenant-menu-create-tenant]").click();
  await page.locator("[name=name]").fill(name);
  await page.getByRole("button", { name: region }).click();
  await page.getByRole("button", { name: env }).click();

  if (complete) {
    await page.getByRole("button", { name: "Create" }).click();
    await page.waitForNavigation();
  } else {
    await page.getByRole("button", { name: "Cancel" }).click();
  }
};
