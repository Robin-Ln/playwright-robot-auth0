import { Page } from "playwright";

export type LoginProps = { url: string; username: string; password: string };

export const login = async (
  page: Page,
  { url, username, password }: LoginProps
) => {
  await page.goto(url);
  await page.getByTestId("username").fill(username);
  await page.keyboard.press("Enter");
  await page.getByTestId("password").fill(password);
  await page.keyboard.press("Enter");
  await page.waitForNavigation();
};
