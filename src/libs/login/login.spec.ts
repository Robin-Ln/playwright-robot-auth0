import { test } from "@playwright/test";
import { login } from "~libs/login";
import { config } from "dotenv";

config();

test("auth0 login", async ({ page }) => {
  await login(page, {
    url: process.env.AUTH0_URL,
    username: process.env.AUTH0_USERNME,
    password: process.env.AUTH0_PASSWORD,
  });
});
