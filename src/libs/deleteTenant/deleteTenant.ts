import { Page } from "playwright";
import { Region } from "~commons/type";

type DeleteTenantProps = {
  url: string;
  region: Region;
  name: string;
  complete: boolean;
};

export const deleteTenant = async (
  page: Page,
  { url, region, name, complete }: DeleteTenantProps
) => {
  await page.goto(`${url}/dashboard/${region}/${name}/tenant/advanced`);
  await page.getByRole("button", { name: "Delete" }).click();

  await page.locator("[name=nameMatch]").fill(name);
  await page.locator("[name=cancel_subscription]").check();
  await page.locator("[name=no_reuse]").check();
  await page.locator("[name=permanent]").check();

  if (complete) {
    await page.getByRole("button", { name: "Yes, delete tenant" }).click();
    await page.waitForResponse((response) => response.ok());
  }
};
