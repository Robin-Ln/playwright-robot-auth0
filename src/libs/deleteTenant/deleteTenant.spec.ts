import { test } from "@playwright/test";
import { login } from "~libs/login";
import { config } from "dotenv";
import { faker } from "@faker-js/faker";
import { deleteTenant } from "./deleteTenant";
import { createTenant } from "~libs/createTenant";

config();

const name = `robot-${faker.internet.domainWord()}`;

test("Delete a tenant", async ({ page }) => {
  await login(page, {
    url: process.env.AUTH0_URL,
    username: process.env.AUTH0_USERNME,
    password: process.env.AUTH0_PASSWORD,
  });

  await createTenant(page, {
    name,
    region: "EU",
    env: "Development",
    complete: true,
  });

  await deleteTenant(page, {
    name,
    region: "eu",
    url: process.env.AUTH0_URL,
    complete: true,
  });
});
