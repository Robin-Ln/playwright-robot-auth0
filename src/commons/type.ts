export type Region = "eu" | never;
export type RegionName = "EU" | never;
export type EnvironmentTag = "development" | never;
export type EnvironmentTagName = "Development" | never;
